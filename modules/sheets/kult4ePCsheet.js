export default class kult4ePCsheet extends ActorSheet {
    get template() {
        return `systems/kult4e/templates/sheets/pc-sheet.hbs`;
    }

    getData() {
        const data = super.getData();
        data.moves = data.items.filter(function (item) {
            return item.type == "move" || "advantage" || "disadvantage" || "darksecret" || "relationship"
        });
        console.log(data);
        return data;
    }


    activateListeners(html) {
        super.activateListeners(html)
        html.find('.item-delete').click(ev => {
            console.log(ev.currentTarget);
            let li = $(ev.currentTarget).parents(".item-name"),
                itemId = li.attr("data-item-id");
            console.log(li);
            console.log(`itemID =0 ${itemId}`);
            this.actor.deleteEmbeddedEntity("OwnedItem", itemId);
        });

        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        html.find('.stability-check').click(ev => {
            stability();
        });

        html.find('.item-create').click(this._onItemCreate.bind(this));



        html.find('.move-roll').click(ev => {
            const li = $(ev.currentTarget).parents(".item-name");
            console.log(li);
            const item = this.actor.getOwnedItem(li.data("itemId"));
            const name = item.name;
            const type = item.data.data.type;
            if (type == "passive") {
                console.log("This ability is Passive");
            } else {
                console.log(`${name}`);
                const attr = item.data.data.attributemod;
                const mod = this.actor.data.data[attr];
                const stab = this.actor.data.data.stability.value;
                console.log(`${stab}`);
                let situation = this.actor.data.data.sitmod + this.actor.data.data.forward;
                console.log(`Situation Modifier is ${situation}`);
                if (name == "Keep It Together" && stab > 3) {
                    situation = situation - 1
                }
                if (name == "Keep It Together" && stab > 6) {
                    situation -= 1
                }
                if (name == "See Through the Illusion" && stab > 6) {
                    situation += 1
                }
                if (name == "Endure Injury") {
                    getAttribute();
                }
                console.log(`${situation}`);
                let r = new Roll(`2d10 + ${mod} + ${situation}`);
                r.roll()
                if (r.total >= 15) {
                    ChatMessage.create({
                        content: "<p>" + item.name + "</p>" + r.result + "<p>" + r.total + "</p>" + item.data.data.completesuccess,
                        speaker: ChatMessage.getSpeaker({alias: this.actor.name})
                    });
                } else if (r.total < 10) {
                    ChatMessage.create({
                        content: "<p>" + item.name + "</p>" + r.result + "<p>" + r.total + "</p>" + item.data.data.failure,
                        speaker: ChatMessage.getSpeaker({alias: this.actor.name})
                    });
                } else {
                    ChatMessage.create({
                        content: "<p>" + item.name + "</p>" + r.result + "<p>" + r.total + "</p>" + item.data.data.partialsuccess,
                        speaker: ChatMessage.getSpeaker({alias: this.actor.name})
                    });
                }
            }

        })

    }

    /**
     * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
     * @param {Event} event   The originating click event
     * @private
     */
    _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        // Get the type of item to create.
        const type = header.dataset.type;
        // Grab any data associated with this control.
        const data = duplicate(header.dataset);
        // Initialize a default name.
        const name = `New ${type.capitalize()}`;
        // Prepare the item object.
        const itemData = {
            name: name,
            type: type,
            data: data
        };
        // Remove the type from the dataset since it's in the itemData.type prop.
        delete itemData.data["type"];

        // Finally, create the item!
        return this.actor.createOwnedItem(itemData);
    }
}